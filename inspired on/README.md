# ¡Mil gracias!

Los siguientes proyectos fueron los que me ayudaron a comprender un poco más de prolog:

- *fruits.pl*: Autor: Paul Brown. Comprendí mejor cómo funciona la lógica del sistema experto y cómo almacenar las respuestas del usuario. Lo encontré en la página de [GOFAI](http://www.paulbrownmagic.com/blog/simple_prolog_expert.html).

- *cars.pl*: Autor: Cristian Arreola. Entendí mucho mejor cómo funciona la creación de interfaz en Prolog.

#### Derechos reservados a sus respectivos autores.