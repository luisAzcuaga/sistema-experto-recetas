:- dynamic nombre_receta/1.

nombre_receta('Pizza') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Pato a la naranja') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Huevos con tocino') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta(barbacoa) :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Barbacoa familiar') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Huevos con tocino') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Sandwich de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Clara de huevo con jamon') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Barbacoa') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Tacos carne asada') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Tacos de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Barbacoa familiar') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Empanada de carne') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Empanada de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Huevos con tocino') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Huevos motule�os') :-
	tipo(desayuno),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Huevos estrellados con BBQ') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Tartaleta de elote') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Barbacoa') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de elote') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Barbacoa familiar') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Pastel de elote') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Tartaletas de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Tartaleta de espinaca') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Tacos carne asada') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Tarta de espinaca') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Empanada de carne') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Rajas poblanas') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sandwich de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Sopa de tomate') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Tacos de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Caldo de tomate') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Empanada de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Caldo de zanahoria') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Huevos con tocino') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Pollo dulce') :-
	tipo(desayuno),
	sabor(dulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Pollo agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Tartaletas de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Tortilla de espinaca') :-
	tipo(desayuno),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Sandwich integral') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Sandwich de pollo') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pechuga BBQ') :-
	tipo(desayuno),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pechuga agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Tartaleta de elote') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Ensalada de zanahoria dulce') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Ensalada agridulce de col') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Tartaleta de espinaca') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Ceviche de jicama') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Taco de lechuga') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Sopa de tomate') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Tomate dulce con tostadas') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Tomate con salsa agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pastel de elote') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de papa en salsa BBQ') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de zanahoria en salsa agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Tarta de espinaca') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de chaya') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Gummadi kai') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Caldo de tomate') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de zanahoria relleno de manzana') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de elote con salsa agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de elote') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Tarta de espinaca y batata') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Tarta de calabaza y ricota con batata') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Rajas poblanas') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Pastel de zanahoria') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Papas agridulces') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Caldo de zanahoria') :-
	tipo(desayuno),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ceviche de coliflor') :-
	tipo(desayuno),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Calabacitas con queso crema agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Sandwich de jamon con pi�a') :-
	tipo(desayuno),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sandwich de pollo agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Choripan') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Asado argentino') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Asado argentino') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Choripan') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Suprema de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Suprema de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Asado argentino') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Bife') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Pollo a la plancha') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Asado argentino') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Pastel de carne') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Pollo asado a la parrilla') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Choripan') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Pollo en salsa dulce') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Pollo agridulce con ajonjoli') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Asado argentino') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Polenta con salsa de tomate') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Asado argentino') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Pastel de acelga') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Croqueta de atun') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Croqueta de acelga') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Bife') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada de puerro') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de carne') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de atun') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Suprema de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Croquetas de acelga y espinaca') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pollo a la plancha') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Costillas BBQ') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Costillas en salsa agridulce') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada mediterranea') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Pollo asado a la parrilla') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de higo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Choripan') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Pierna envinada') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Pollo agridulce') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Croqueta de atun') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Milanesa de puero') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Pollo agridulce') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Suprema de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pechuga a la plancha') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pollo agridulce') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Ensalada de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Croqueta de espinaca y zanahoria') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Champignon agridulce') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Croqueta de acelga') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Ensalada de papa y ciruela') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Coctel de salchichas') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Croquetas de acelga y espinaca') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Hamburguesa de pollo dulce') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pionono agridulce de champignon') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Polenta con salsa de tomate') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada mediterranea con arrachera') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada de tuna con esparragos') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada de puerro') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada de zanahoria') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada de berros') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada mediterranea') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Ensalada fresca mexicana') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de espinaca con salsa agridulce') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Pastel de acelga') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de maiz dulce') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada agridulce cubana') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de atun') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de escarola y ajo') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de judias verdes') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de higo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de remolacha') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de canonigos agridulces') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Tacos al pastor') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Nachos al pastor') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Parrillada') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Tacos al pastor') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Sandwich de jamon de pavo') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Sandwich de jamon serrano') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Nachos al pastor') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Tortas de carne asada') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Sandwiches de pollo') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Parrillada') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Lubina') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Lubina') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Tacos al pastor') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Taco de lechuga') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Nachos al pastor') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Champignon en jugo maggi') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Parrillada') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ceviche de champignon') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de pollo y surimi') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Chiltamal') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Torta carne asada') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Ceviche de soya') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Lubina') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ceviche de soya') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sandwich de jamon de pavo') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Croqueta de espinaca') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Sandwich de pollo') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Crema de zanahoria y queso') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Lubina') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada verde') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Tacos al pastor') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Empanada de picadillo dulce') :-
	tipo(cena),
	sabor(dulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Albondigas en salsa agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Ensalada de pollo y surimi') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Hamburguesa de pollo con cebolla caramelizada') :-
	tipo(cena),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Alitas agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Sandwich de jamon de pavo') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Hamburguesa de pollo con cebolla caramelizada') :-
	tipo(cena),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Tacos de res en salsa agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Pechuga de pollo caramelizada') :-
	tipo(cena),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Pechuga de pollo en salsa agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Taco de lechuga') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Ensalada de repollo dulce') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Ensalada de alubias agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Chiltamal') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Ensalada dulce de repollo') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Sopa de esparrago') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(solo_yo).
nombre_receta('Croqueta de espinaca') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Sopa de tomate') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Sopa de verdura') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Champignon en jugo maggi') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Pizza vegana de rucula') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Pizza vegana de rucula') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Ceviche de soya') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Lasagna de espinaca') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Lasagna de rucula') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Crema de zanahoria y queso') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Burrito vegano') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Burrito vegano en salsa agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Ceviche de champignon') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada fria de caracolitos') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de remolacha agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ceviche de soya') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada fria de caracoles') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de remolacha agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada verde') :-
	tipo(cena),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada fria de caracoles') :-
	tipo(cena),
	sabor(dulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada de remolacha agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Pechuga de pollo caramelizada') :-
	tipo(cena),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Pechuga de pollo en salsa agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sandwich de jamon con pi�a') :-
	tipo(desayuno),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Sandwich de pollo agridulce') :-
	tipo(desayuno),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Sandwich de jamon con pi�a') :-
	tipo(almuerzo),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Sandwich de pollo agridulce') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Sandwich de jamon con pi�a') :-
	tipo(cena),
	sabor(dulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Sandwich de pollo agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(diabetes),
	cantidad(hasta_cuatro).
nombre_receta('Sandwich de jamon con pi�a') :-
	tipo(cena),
	sabor(dulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Sandwich de pollo agridulce') :-
	tipo(cena),
	sabor(agridulce),
	origen(animal),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).

