:-use_module(library(pce)).
:-consult('conocimiento.pl').
:- pce_image_directory('./imagenes').

%Fondo de la interfaz principal
resource(cocina,image,image('cocina.jpg')).

resource(huevo_tocino, image, image('huevo_tocino.jpg')).



inicio:-
	new(Menu, dialog('Nombres de Recetas | Sistema Experto')),
	new(Title,label(nombre,'[VAMO A COMEE]',bold)),
	new(InstructionsText_1,label(nombre,'Responde unas preguntas y te ayudo a elegir qu� comer :)')),
	new(ButtonStart,button('�Empecemos!',message(@prolog,preguntas))),
	new(ButtonAdd,button('Agregar info', message(@prolog,aprender))),
	new(@resHeader,label(nombre,'')),
	new(@respl,label(nombre,'[AQUI VA LA SUGERENCIA]',bold)),
	new(ButtonExplain,button('Explicar',message(@prolog,explain))),
	new(Imagen,label(icon,resource(cocina))),
	new(MadeBy_1,label(nombre,'Hecho por')),
	new(MadeBy_2,label(nombre,'Fernando Coraggio, Fernando Ley')),
	new(MadeBy_3,label(nombre,'y Luis Azcuaga')),

	new(Salir,button('SALIR',
		and(
			message(Menu, destroy),message(Menu,free),
			message(InstructionsText_1,free),
			message(ButtonStart,free),
			message(ButtonAdd,free),
			message(@resHeader,free),
			message(@respl,free)
		)
	)),

	send(Menu,size,size(460,630)),
	send(Menu,display,InstructionsText_1,point(20,50)),
	send(Menu,display,Title,point(170,10)),
	send(Menu,display,ButtonStart,point(100,90)),
	send(Menu,display,ButtonAdd,point(220,90)),
	send(Menu,display,@resHeader,point(20,130)),
	send(Menu,display,@respl,point(20,150)),
	send(Menu,display,ButtonExplain,point(300,150)),
    send(Menu,display,Imagen,point(15,200)),
	send(Menu,display,MadeBy_1,point(20,550)),
	send(Menu,display,MadeBy_2,point(20,570)),
	send(Menu,display,MadeBy_3,point(20,590)),
	send(Menu,open_centered),
	send(Menu,display,Salir,point(300,570)).


preguntas :-
	clearData,
	nombre_receta(Comida),
	send(@resHeader,selection('Deber�as de preparar: ')),
	%In the end, this will be returned to main window
	assert(respuesta(receta,Comida)),
	send(@respl,selection(Comida))
	;
	send(@resHeader,selection(':(')),
	send(@respl,selection('No tengo tanto conocimiento.')).

%This tells prolog that respuesta will grow overtime
:- dynamic respuesta/2.

%This is used to clear all user received data
clearData:- retract(respuesta(_,_)),fail.
clearData.

%This calls each question
tipo(X):- pregunta1('�Ser� desayuno,almuerzo o cena?', X),!.
sabor(X):- pregunta2('�Qu� sabor buscas?', X),!.
origen(X):- pregunta3('�De qu� origen quieres?', X),!.
padecimiento(X):- pregunta4('�Tienes algun padecimiento?', X),!.
cantidad(X):- pregunta5('�Para cu�ntas personas ser�?', X),!.


explain :- new(VentanaExplicacion, dialog('Explicacion')),

	%respuesta(receta,X),
	%atom_concat(X, '.jpg', Resource),
	%resource(imagenFinal, image, image(Resource)),
	%print(imagenFinal),
	%new(Imagen,label(icon,resource(imagenFinal))),
	%send(VentanaExplicacion,display,Imagen,point(0,0)),

	respuesta('�Ser� desayuno,almuerzo o cena?',Res1),
	respuesta('�Qu� sabor buscas?',Res2),
	respuesta('�De qu� origen quieres?',Res3),
	respuesta('�Tienes algun padecimiento?',Res4),
	respuesta('�Para cu�ntas personas ser�?',Res5),
	new(ButtonExit, button('Ok :)', message(VentanaExplicacion, destroy))),

	send(VentanaExplicacion, append(new(label(nombre,'El usuario previamente eligi�:')))),
	send(VentanaExplicacion, append(new(label(nombre,Res1,bold)))),
	send(VentanaExplicacion, append(new(label(nombre,Res2,bold)))),
	send(VentanaExplicacion, append(new(label(nombre,Res3,bold)))),
	send(VentanaExplicacion, append(new(label(nombre,Res4,bold)))),
	send(VentanaExplicacion, append(new(label(nombre,Res5,bold)))),
	send(VentanaExplicacion, append,ButtonExit),
	send(VentanaExplicacion, open_centered).

pregunta1(Pregunta,Val):- respuesta(Pregunta,Val),!.
pregunta1(Pregunta,Val):- respuesta(Pregunta,Val),!,fail.
pregunta1(Pregunta,Val):- respuesta(Pregunta,V),V \==Val,!,fail.
pregunta1(Pregunta,Val):-
	new(VentanaPregunta, dialog('Pregunta')),
	new(PreguntaLabel,label(nombre,Pregunta)),
	new(Opcion_1,button(desayuno, and(message(VentanaPregunta,return,desayuno)))),
	new(Opcion_2,button(almuerzo, and(message(VentanaPregunta,return,almuerzo)))),
	new(Opcion_3,button(cena, and(message(VentanaPregunta,return,cena)))),

	send(VentanaPregunta,append(PreguntaLabel)),
	send(VentanaPregunta,append(Opcion_1)),
	send(VentanaPregunta,append(Opcion_2)),
	send(VentanaPregunta,append(Opcion_3)),

	send(VentanaPregunta,default_button,desayuno),
	send(VentanaPregunta,open_centered),
	get(VentanaPregunta,confirm,Respuesta),

	assert(respuesta(Pregunta,Respuesta)),
	send(VentanaPregunta,destroy),
	Respuesta==Val.

pregunta2(Pregunta,Val):- respuesta(Pregunta,Val),!.
pregunta2(Pregunta,Val):- respuesta(Pregunta,Val),!,fail.
pregunta2(Pregunta,Val):- respuesta(Pregunta,V),V \==Val,!,fail.
pregunta2(Pregunta,Val):-
	new(VentanaPregunta, dialog('Pregunta')),
	new(PreguntaLabel,label(nombre,Pregunta)),
	new(Opcion_1,button(salado, and(message(VentanaPregunta,return,salado)))),
	new(Opcion_2,button(dulce, and(message(VentanaPregunta,return,dulce)))),
	new(Opcion_3,button(agridulce, and(message(VentanaPregunta,return,agridulce)))),

	send(VentanaPregunta,append(PreguntaLabel)),
	send(VentanaPregunta,append(Opcion_1)),
	send(VentanaPregunta,append(Opcion_2)),
	send(VentanaPregunta,append(Opcion_3)),

	send(VentanaPregunta,default_button,salado),
	send(VentanaPregunta,open_centered),
	get(VentanaPregunta,confirm,Respuesta),

	assert(respuesta(Pregunta,Respuesta)),
	send(VentanaPregunta,destroy),
	Respuesta==Val.

pregunta3(Pregunta,Val):- respuesta(Pregunta,Val),!.
pregunta3(Pregunta,Val):- respuesta(Pregunta,Val),!,fail.
pregunta3(Pregunta,Val):- respuesta(Pregunta,V),V \==Val,!,fail.
pregunta3(Pregunta,Val):-
	new(VentanaPregunta, dialog('Pregunta')),
	new(PreguntaLabel,label(nombre,Pregunta)),
	new(Opcion_1,button(animal, and(message(VentanaPregunta,return,animal)))),
	new(Opcion_2,button(vegetal, and(message(VentanaPregunta,return,vegetal)))),

	send(VentanaPregunta,append(PreguntaLabel)),
	send(VentanaPregunta,append(Opcion_1)),
	send(VentanaPregunta,append(Opcion_2)),

	send(VentanaPregunta,default_button,animal),
	send(VentanaPregunta,open_centered),
	get(VentanaPregunta,confirm,Respuesta),

	assert(respuesta(Pregunta,Respuesta)),
	send(VentanaPregunta,destroy),
	Respuesta==Val.

pregunta4(Pregunta,Val):- respuesta(Pregunta,Val),!.
pregunta4(Pregunta,Val):- respuesta(Pregunta,Val),!,fail.
pregunta4(Pregunta,Val):- respuesta(Pregunta,V),V \==Val,!,fail.
pregunta4(Pregunta,Val):- %padecimiento
	new(VentanaPregunta, dialog('Pregunta')),
	new(PreguntaLabel,label(nombre,Pregunta)),
	new(Opcion_1,button(ninguno, and(message(VentanaPregunta,return,ninguno)))),
	new(Opcion_2,button(diabetes, and(message(VentanaPregunta,return,diabetes)))),
	new(Opcion_3,button(hipertension, and(message(VentanaPregunta,return,hipertension)))),

	send(VentanaPregunta,append(PreguntaLabel)),
	send(VentanaPregunta,append(Opcion_1)),
	send(VentanaPregunta,append(Opcion_2)),
	send(VentanaPregunta,append(Opcion_3)),

	send(VentanaPregunta,default_button,ninguno),
	send(VentanaPregunta,open_centered),
	get(VentanaPregunta,confirm,Respuesta),

	assert(respuesta(Pregunta,Respuesta)),
	send(VentanaPregunta,destroy),
	Respuesta==Val.

pregunta5(Pregunta,Val):- respuesta(Pregunta,Val),!.
pregunta5(Pregunta,Val):- respuesta(Pregunta,Val),!,fail.
pregunta5(Pregunta,Val):- respuesta(Pregunta,V),V \==Val,!,fail.
pregunta5(Pregunta,Val):- %cantidad
	new(VentanaPregunta, dialog('Pregunta')),
	new(PreguntaLabel,label(nombre,Pregunta)),
	new(Opcion_1,button(solo_yo, and(message(VentanaPregunta,return,solo_yo)))),
	new(Opcion_2,button(hasta_cuatro, and(message(VentanaPregunta,return,hasta_cuatro)))),
	new(Opcion_3,button(mas_de_cuatro, and(message(VentanaPregunta,return,mas_de_cuatro)))),

	send(VentanaPregunta,append(PreguntaLabel)),
	send(VentanaPregunta,append(Opcion_1)),
	send(VentanaPregunta,append(Opcion_2)),
	send(VentanaPregunta,append(Opcion_3)),

	send(VentanaPregunta,default_button,solo_yo),
	send(VentanaPregunta,open_centered),
	get(VentanaPregunta,confirm,Respuesta),

	assert(respuesta(Pregunta,Respuesta)),
	send(VentanaPregunta,destroy),
	Respuesta==Val.

aprender:-
	new(VentanaAprender, dialog('Aprendiendo...')),

	new(Tipo, menu('�Para qu� horario es la nueva comida?')),
	new(Sabor, menu('�Qu� sabor tiene la nueva comida?')),
	new(Origen, menu('�De qu� origen es la nueva comida?')),
	new(Padecimiento, menu('�Es para alg�n tipo de padecimiento esta nueva comida?')),
	new(Cantidad, menu('�Para cu�ntas personas es la nueva comida?')),
	new(ButtonExit, button('CANCELAR', message(VentanaAprender, destroy))),
    send(VentanaAprender, append(new(NameItem, text_item('Nombre comida')))),
	new(ButtonAdd, button('�Agregar conocimiento!', and(
		message(
			@prolog,guardarConocimiento,
			NameItem?selection,
			Tipo?selection,
			Sabor?selection,
			Origen?selection,
			Padecimiento?selection,
			Cantidad?selection
		),
		message(
			VentanaAprender,destroy
		)))),

	send_list(Tipo,append,['desayuno','almuerzo','cena']),
	send_list(Sabor,append,['salado','dulce','agridulce']),
	send_list(Origen,append,['animal','vegetal']),
	send_list(Padecimiento,append,['ninguno','hipertension','diabetes']),
	send_list(Cantidad,append,['solo_yo','hasta_cuatro','mas_de_cuatro']),

	send_list(VentanaAprender, append,[Tipo,Sabor,Origen,Padecimiento,Cantidad]),
	send(VentanaAprender,append,ButtonExit),
	send(VentanaAprender,append,ButtonAdd),
	send(VentanaAprender, open_centered).

guardarConocimiento(Nombre,A,B,C,D,E):-

	asserta((nombre_receta(Nombre):-
		tipo(A),
		sabor(B),
		origen(C),
		padecimiento(D),
		cantidad(E))),
		tell('conocimiento.pl'),
		listing(nombre_receta),								told.

