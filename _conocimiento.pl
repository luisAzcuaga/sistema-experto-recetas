:- dynamic nombre_receta/1.


nombre_receta('huevos con tocino') :-
	tipo(desayuno),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Panuchos') :-
	tipo(desayuno),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Salbutes') :-
	tipo(desayuno),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Hot cakes') :-
	tipo(desayuno),
	sabor(dulce),
	origen(trigo),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Panqueques') :-
	tipo(desayuno),
	sabor(dulce),
	origen(trigo),
	padecimiento(hipertension),
	cantidad(hasta_cuatro).
nombre_receta('Albondigas de ternera') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Asado argentino') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Camaron al mojo de ajo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Pato a la naranja') :-
	tipo(almuerzo),
	sabor(agridulce),
	origen(animal),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Milanesa') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Empanada de carne') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Consomé de carnero') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Consomé de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Sopa de verduras') :-
	tipo(almuerzo),
	sabor(salado),
	origen(verdura),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sopa de esparragos') :-
	tipo(almuerzo),
	sabor(salado),
	origen(verdura),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sopa de tomate') :-
	tipo(almuerzo),
	sabor(salado),
	origen(verdura),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sopa de zanahoria') :-
	tipo(almuerzo),
	sabor(salado),
	origen(verdura),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada mediterranea') :-
	tipo(almuerzo),
	sabor(salado),
	origen(verdura),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Pastel de papa') :-
	tipo(almuerzo),
	sabor(salado),
	origen(verdura),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Ensalada') :-
	tipo(almuerzo),
	sabor(salado),
	origen(verdura),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Tacos') :-
	tipo(almuerzo),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Lasagna') :-
	tipo(almuerzo),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Pasta') :-
	tipo(almuerzo),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Ravioles') :-
	tipo(almuerzo),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Polenta con salsa de tomate') :-
	tipo(almuerzo),
	sabor(salado),
	origen(trigo),
	padecimiento(hipertension),
	cantidad(mas_de_cuatro).
nombre_receta('Burrito vegano') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Ensalada de pollo') :-
	tipo(almuerzo),
	sabor(salado),
	origen(vegetal),
	padecimiento(hipertension),
	cantidad(solo_yo).
nombre_receta('Hamburguesa de res') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Milanesa') :-
	tipo(cena),
	sabor(salado),
	origen(animal),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Sopa de verduras') :-
	tipo(cena),
	sabor(salado),
	origen(verdura),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Sopa de esparragos') :-
	tipo(cena),
	sabor(salado),
	origen(verdura),
	padecimiento(diabetes),
	cantidad(mas_de_cuatro).
nombre_receta('Pizza') :-
	tipo(cena),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(mas_de_cuatro).
nombre_receta('Tacos') :-
	tipo(cena),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Pasta') :-
	tipo(cena),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(hasta_cuatro).
nombre_receta('Panuchos') :-
	tipo(cena),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(solo_yo).
nombre_receta('Salbutes') :-
	tipo(cena),
	sabor(salado),
	origen(trigo),
	padecimiento(ninguno),
	cantidad(solo_yo).

